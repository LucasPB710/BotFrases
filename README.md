# O q é isso?

teoricamente isso é um bot q gera frases aleatorias

----------------

# Como contribuir

obs.: todos os arquivos importantes estão em public/

meio que fodase o que ta em baixo, caso tu queira contribuir, tu pode adicionar uma sugestão [aqui](hhttps://app.suggestionox.com/r/NID2yh)

ok, eu tlg q ler é chato, então eu fiz um video exemplificando o processo todo, mas leia pelo menos a parte das listas f1, f2 e f3 pra saber o q adicionar e como o bot funciona

enfim, aqui esta [o video](https://youtu.be/G4ICLNZE0bw) mostrando como adicionar algo ao bot

pra contribuir, basta adicionar uma frase à lista com o nome "f1" "f2" e "f3", seguindo o padrão das outras frases nelas

sinta-se à vontade pra adicionar coisa bizarra, coisa religiosa, coisa sobre a produção de tubarões artificiais, qualquer coisa será aceita

caso você tenha uma ideia, porem n conseguiu pensar em como encaixar ela nos padrões (de beleza da sociedade moderna) das outras frases, não tem problema, alguem (ou eu, porem preguiça, então duvido um pouco disso) irá conseguir encaixar ela lá

## Lista F1

a lista f1 contem frases iniciais, como:

- Bom dia,
- Ao acordar,

e frases do tipo

## Lista F2

essa lista contem mais coisas que alguem deve fazer, como verbos tipo

- embale
- corra de
- lembre-se de dar um abraço em
- No jantar romantico, conte uma historia de terro envolvendo 
  (eu tirei isso da minha cabeça, então é provavel que elas não estejam no programa)

## Lista F3

essa lista contem nome de pessoas, lugares, ou qualquer coisa que seja algo, tipo

- fogao de 8 bocas
- aviao de tripla helice
- caue
- cozinheiro
- capitalismo
- comunismo
- lula bombado (preciso adicionar isso no programa)
- giz de cera
- renan

é isso ai, pode ser literalmente qualquer coisa, seu personagem preferido, banda preferida, cantor preferido, desenho preferido, baralho preferido, pedra preferida, animal preferido e por ai vai

----------------

# como funciona

assim, isso n é bem um bot bot msm, ele so pega frases aleatorias de uma lista de frases e mistura elas aleatoriamente, tipo

lista 1: "Ao acordar, ", "Cuidado ao atravessar a rua, "

lista 2: "dê um banho em ", "cheire o cangote de "

lista 3: "cozinheiro", "escapamento de moto "

podendo gerar, por exemplo: "ao acordar, dê um banho em escapamento de moto"

alem disso, tem uma chance de 50% de ele extender a frase, adicionando um "e" ou outros conectivos e adicionar algo da lista 2 e lista 3 novamente

por exemplo: "cuidado ao atravessar a rua, cheire o cangote de cozinheiro e dê um banho em escapamento de moto"

------------------

# Creditos

Como o povo ta pouco se fudendo pra quem faz as coisas, como sempre, os creditos às pessoas vão ficar no final.



Contribuições com sugestões:

- [Charlesdarwin](https://github.com/charlesrdarwin)

- mill (foi fora do github, então F link)
  
  

Contribuições com codigo:

# absolutamente ninguem :)

------------------------

## Conclusão

ONOMATOPEIAS SÃO MENTIRAS!
